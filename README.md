Para ver la animación simplemente abrir `index.html` dentro de `src`.

Para uso con docker, copiar `docker-compose.yml.example` a `docker-compose.yml` y hacer

`docker-compose up`

Y navegar a http://localhost:4000/

Código basado en https://github.com/cupcakearmy/docker-nginx-static-server
